#!/bin/bash

cd $(mktemp -d)

mkdir receiver
cd ./receiver
#touch file1 file4 file5
echo foo baaaar > file1
echo bar > file2
echo foo bar 3 > file3
echo foo bar > file4
echo foo bar > file5
ls -al

timeout 116 /home/michal/rsync_mir/build/rsync recv 32040 . &

cd ..
mkdir sender
cd ./sender
# touch file1 file2 file3
echo foo bar 1 > file1
echo foo bar 2 > file2
echo foo bar 3 > file3
echo 1234567 > file7

#create big random bin file  -- /tmp/t
dd if=/dev/urandom of=/tmp/t bs=1 count=1M
mv /tmp/t bin_file1
ls -al

timeout 116 /home/michal/rsync_mir/build/rsync send localhost 32040 .

wait
echo "Compare to directories"
diff -y ../sender ../receiver

#rm -r ../sender ../receiver

#echo "difference is"
#diff -y /tmp/sender /tmp/rec
#cd /home/michal/rsync_mir/build/
#make -j 4
# ./rsync-server recv 32040 /tmp/rec &
#timeout 10 ./rsync-server recv 32040 /tmp/rec &
#./rsync-server send localhost 32040 /tmp/sender
#diff -y /tmp/sender /tmp/rec

