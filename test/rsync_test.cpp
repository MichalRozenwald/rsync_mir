#include "rsync.h"
#include <thread>
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <gtest/gtest.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <vector>
#include <utility>
#include <md5.h>

int mkdir(const char *path, mode_t mode);

class FakeConnection : public Connection {
public:
    Frame frame;
    virtual void WriteFrame(Frame* f) { frame = *f; } //
    virtual void ReadFrame(Frame* f) { *f = frame; } //
};


TEST(FakeConnection, RemembersFrame) {
    FakeConnection c;
    Frame f;
    f.msg_id = MsgId::GETLIST;
    c.WriteFrame(&f);
    c.ReadFrame(&f);

    ASSERT_EQ(MsgId::GETLIST, c.frame.msg_id);
}

TEST(Protocol, GetList) {
    FakeConnection c;
    Protocol p(&c);

    p.SendGetList();
    ASSERT_EQ(MsgId::GETLIST, c.frame.msg_id);
    ASSERT_EQ(MsgId::GETLIST, p.RecvMsg());
}

TEST(Protocol, FileList) {
    FakeConnection c;
    Protocol p(&c);

    FileList l;
    l.files = { "a.txt", "b.txt", "c.txt" };

    p.SendFileList(l);
    ASSERT_EQ(MsgId::FILELIST, c.frame.msg_id);
    ASSERT_EQ(MsgId::FILELIST, p.RecvMsg());

    ASSERT_EQ(l.files, p.GetFileList().files);
}

TEST(SocketConnection, ReadWrite) {
    int socks[2];
    ASSERT_EQ(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks)); // check sock connection

    SocketConnection sender(socks[0]), receiver(socks[1]);

    Frame f1, f2;
    f1.msg_id = MsgId::OK;
    f1.body.resize(1024, 'f');

    sender.WriteFrame(&f1);
    receiver.ReadFrame(&f2);

    ASSERT_EQ(f1.msg_id, f2.msg_id);
    ASSERT_EQ(f1.body, f2.body);
}

TEST(Thread, Example) {
    std::thread t1([&] {
        std::cout << "1" << std::endl;
        usleep(20000);
        std::cout << "2" << std::endl;
    });

    std::thread t2([&] {
        usleep(10000);
        std::cout << "3" << std::endl;
        usleep(20000);
        std::cout << "4" << std::endl;
    });
    t1.join();
    t2.join();
}

TEST(DirectoryListPath, FileList) {
    std::string pathname = "/home/michal/rsync_mir";
    FileList fl = DirectoryList(pathname);

    for (std::string file_name : fl.files) {
        std::cout << file_name << std::endl;
    }
}

TEST(DirectoryList, CreateCheck) {

    std::string new_dir = "/home/michal/rsync_mir/check_dir";
    const char *first_dir = new_dir.c_str();

    mkdir(first_dir,0777);

    new_dir = "/home/michal/rsync_mir/check_dir/file2";
    const char *file_1 = new_dir.c_str();
    mkdir(file_1,0777);

    new_dir = "/home/michal/rsync_mir/check_dir/file1";
    const char *file_2 = new_dir.c_str();
    mkdir(file_2,0777);

    FileList fl = DirectoryList("/home/michal/rsync_mir/check_dir"); // DirectoryListPath returnes SORTED file list

    rmdir(file_1);
    rmdir(file_2);
    rmdir(first_dir);

    for (std::string file_name : fl.files) {
        std::cout << file_name << std::endl;
    }
    ASSERT_EQ(fl.files.size(), 2);
    ASSERT_EQ(fl.files[0], "file1");
}


TEST(CheckDirFileList, RmCpFiles) {
    std::string new_dir = "/home/michal/rsync_mir/check_dir_1";
    const char *first_dir = new_dir.c_str();
    mkdir(first_dir, 0777);

    new_dir = "/home/michal/rsync_mir/check_dir_1/file2";
    const char *file_1 = new_dir.c_str();
    mkdir(file_1, 0777);
    new_dir = "/home/michal/rsync_mir/check_dir_1/file1";
    const char *file_2 = new_dir.c_str();
    mkdir(file_2, 0777);
    new_dir = "/home/michal/rsync_mir/check_dir_1/file5";
    const char *file_3 = new_dir.c_str();
    mkdir(file_3, 0777);

    FileList fl_1 = DirectoryList(
        "/home/michal/rsync_mir/check_dir_1"); // DirectoryListPath returnes SORTED file list

    rmdir(file_1);
    rmdir(file_2);
    rmdir(file_3);
    rmdir(first_dir);

    std::cout << "files from fl_1:" << std::endl;
    for (std::string file_name : fl_1.files) {
        std::cout << file_name << std::endl;
    }

    new_dir = "/home/michal/rsync_mir/check_dir_2";
    first_dir = new_dir.c_str();

    mkdir(first_dir, 0777);

    new_dir = "/home/michal/rsync_mir/check_dir_2/file4";
    file_1 = new_dir.c_str();
    mkdir(file_1, 0777);
    new_dir = "/home/michal/rsync_mir/check_dir_2/file2";
    file_2 = new_dir.c_str();
    mkdir(file_2, 0777);

    std::cout << "files from fl_2:" << std::endl;
    FileList fl_2 = DirectoryList(
        "/home/michal/rsync_mir/check_dir_2");

    rmdir(file_1);
    rmdir(file_2);
    rmdir(first_dir);

    for (std::string file_name : fl_2.files) {
        std::cout << file_name << std::endl;
    }
    ASSERT_EQ(fl_1.files.size(), 3);
    ASSERT_EQ(fl_2.files.size(), 2);
    ASSERT_EQ(fl_1.files[0], "file1");

    FileList rm, cp, same;
    std::vector<FileList> rm_cp_same = CheckDirFileList(fl_1, &rm, &cp, &same,fl_2);
    PrintComToSyncDirs(rm_cp_same);
}


TEST(FileHashes, HashMd5) {
    int fd_1 = open(".", O_RDONLY);
    int fd_2 = open(".", O_RDONLY);
    int fd_3 = open("/home/michal/rsync_mir/main.cpp", O_RDONLY);

    std::string hash_1 = FileHash(fd_1);
    std::string hash_2 = FileHash(fd_2);
    std::string hash_3 = FileHash(fd_3);
    ASSERT_EQ(hash_1, hash_2);
    GTEST_ASSERT_NE(hash_1, hash_3);

    std::vector<std::string> files_in_dir;
    files_in_dir.push_back(".");
    files_in_dir.push_back("main.cpp");
    std::string path_to_dir = "/home/michal/rsync_mir";
    FilesHashes files_hashes = CountFilesHashes(files_in_dir, path_to_dir);
    ASSERT_EQ(files_hashes.files_hashes[0].size(), files_hashes.files_hashes[1].size());

    for (int file = 0; file < files_hashes.files_hashes[0].size(); file++) {
        std::cout << files_hashes.files_hashes[0][file] << "'s hash = " << files_hashes.files_hashes[1][file] << std::endl;
    }
}
