project(hse-sample-project)
cmake_minimum_required(VERSION 2.8)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -g -std=c++11")

include_directories(.)

add_library(rsync-lib
  rsync.cpp
  connection_types.cpp
  md5.cpp
)

add_executable(rsync
  main.cpp
)
target_link_libraries(rsync rsync-lib pthread boost_serialization)

include_directories(contrib)

add_executable(run_test
  contrib/gmock-gtest-all.cc
  contrib/gmock_main.cc
  test/rsync_test.cpp
)
target_link_libraries(run_test rsync-lib pthread boost_serialization)
