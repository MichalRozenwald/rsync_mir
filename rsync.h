#pragma once

#include <vector>
#include <string>
#include <utility>

#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>


enum class MsgId {
    GETLIST = 1,
    FILELIST = 2,
    REMOVE = 3,
    REMOVED_OK  = 4,
    CHECK_SAME = 5,
    DIF_FILES = 6,
    FILE_DESCR= 7,
    FILE_CREATED_OK = 8,
    FILE_PART = 9,
    FILE_PART_OK = 10,
    OK = 11
};

class Frame {
public:
    void WriteFileToFrame(int fd);
    void WriteFrameToFile(int fd);

    MsgId msg_id;
    bool last = false;
    std::string body;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & body;
    }
};

struct FileList {
    std::vector<std::string> files;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & files;
    }
};

struct FileDescr {
    std::string file_name;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & file_name;
    }
};


struct FilesHashes {
    std::vector<std::vector<std::string>> files_hashes;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int version) {
        ar & files_hashes;
    }
};


class Connection {
public:
    virtual ~Connection() {}
    virtual void WriteFrame(Frame* frame) = 0;
    virtual void ReadFrame(Frame* frame) = 0;
};

class SocketConnection : public Connection {
public:
    SocketConnection(int fd) : fd_(fd) {}
    ~SocketConnection() { close(fd_); }

    virtual void WriteFrame(Frame* frame) override;
    virtual void ReadFrame(Frame* frame) override;

private:
    int fd_;
};


class Protocol {
public:
    Protocol(Connection* conn) : conn_(conn) {}

    void SendGetList();
    void SendFileList(const FileList& list);

    void SendRemoveAndFileList(const FileList& remove_list);
    FileList GetFileList();
    void SendRemovedOk();

    void SendSameFileHashes(const FilesHashes& files_hashes);
    FilesHashes GetSameFilesHashes();
    void SendDifFileList(const FileList& list);

    void SendFileDescr(const FileDescr file_info);
    FileDescr GetFileDescr();
    void SendFileCreatedOk();

    void SendFilePart(const Frame file_part);
    Frame GetFilePart();
    void SendFilePartOk();

    void SendOk();

    MsgId RecvMsg();
    Frame LastFrame();
    void CheckMsgError(MsgId check_msg);
    bool CheckMsg(MsgId msg);

private:
    Connection* conn_;
    Frame last_received_;
};

class Receiver {
public:
    Receiver(int fd, std::string path) : sock_(fd), proto_(&sock_), path_(path)  {}

    void Work();

private:
    SocketConnection sock_;
    Protocol proto_;
    std::string path_;
};

class Sender {
public:
    Sender(int fd, std::string path) : sock_(fd), proto_(&sock_), path_(path) {}

    void Work();
    void SendFiles(std::vector<std::string> file_names);

private:
    SocketConnection sock_;
    Protocol proto_;
    std::string path_;
};


FileList DirectoryList(std::string pathname);
std::vector<FileList> CheckDirFileList(FileList send_file_list, FileList *rm_p, FileList *cp_p, FileList *same_p, FileList rec_file_list);
void Remove(FileList rm_file_lists, std::string path);

std::vector<std::string> PrintComToSyncDirs(std::vector<FileList> rm_cp_same_file_lists);

std::string FileHash(int fd);
FilesHashes CountFilesHashes(std::vector<std::string> files_in_dir, std::string path_to_dir);
FileList CheckSameFilesHashes(FilesHashes same_files, std::string path_to_comp_dir);
