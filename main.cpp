#include <iostream>
#include <rsync.h>
#include <connection_types.h>
#include <md5.h>


void usage() { // my help
    std::string usage = "usage: rsync send HOST PORT SRC_DIR\n"
                        "       rsync recv PORT DST_DIR\n"
                        "       rsync local SRC_DIR DST_DIR\n";
    std::cerr << usage;
    exit(1);
}

// host == 127.0.0.1 <= localhost - domen name

int main(int argc, char * argv[]) {
    if (argc < 2 || std::string(argv[1]) == "--help") usage();

    std::string option = argv[1];

    if (option == "local") {
        if (argc != 4) usage();

        std::string src = argv[2];
        std::string dst = argv[3];
        MakeLocalConnection(src, dst);

    } else if (option == "send") { // client
        if (argc != 5) usage();

        std::cerr << "Creating SENDER connection" << std::endl;
        std::string host = argv[2];
        std::string port = argv[3]; 
        std::string src_dir = argv[4];
        ConnectSender(host, port, src_dir); //client

    } else if (option == "recv") { // server
        if (argc != 4) usage();

        std::cerr << "Creating RECEIVER connection" << std::endl;
        std::string port = argv[2];
        std::string dst_dir = argv[3];
        ConnectReceiver(port, dst_dir); //server
    } else usage();

    return 0;
}
