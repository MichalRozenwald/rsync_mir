#include "rsync.h"
#include "md5.h"

#include <sstream>
#include <stdexcept>
#include <system_error>

#include <iostream>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#include<unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <cstdio>
#include <utility>


/*
    ```

   *------*               *--------*
   |Sender|               |Receiver|
   *------*               *--------*
      ||    1) GETLIST        ||
      ||    ------------>     ||
      ||    2) FILELIST       ||
      ||    <------------     ||
      ||    3) REMOVE         ||
      ||    ------------>     ||
      ||    4) REMOVED_OK     ||
      ||    <------------     ||
      ||    5) CHECK_SAME     ||
      ||    ------------>     ||
      ||    6) DIF_FILES      ||
      ||    <------------     ||
      ||                      ||
      ||    7) FILE_DESCR     ||
      ||    ------------>     ||
      ||    8)FILE_CREATED_OK ||
      ||    <------------     ||
      ||    9) FILE_PART      ||
      ||    ------------>     ||
      ||    10) FILE_PART_OK  ||
      ||    <------------     ||
      ||   --REPEAT 5..8--    ||
      ||                      ||
      ||    11) OK            ||
      ||    ------------>     ||


   ```
*/

void Protocol::SendGetList() {  // Sender ask Receiver list of files - sending a request
    Frame f;
    f.msg_id = MsgId::GETLIST;
    conn_->WriteFrame(&f);
}

void Protocol::SendFileList(const FileList& list) {
    Frame f;
    f.msg_id = MsgId::FILELIST;

    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << list;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

FileList Protocol::GetFileList() {
    std::stringstream ss;
    ss << last_received_.body;

    FileList list;
    boost::archive::text_iarchive archive(ss);
    archive >> list;
    return list;
}


void Protocol::SendRemoveAndFileList(const FileList& remove_list) {
    Frame f;
    f.msg_id = MsgId::REMOVE;

    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << remove_list;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

void Protocol::SendRemovedOk() {
    Frame f;
    f.msg_id = MsgId::REMOVED_OK;
    conn_->WriteFrame(&f);
}

void Protocol::SendSameFileHashes(const FilesHashes& files_hashes) {
    Frame f;
    f.msg_id = MsgId::CHECK_SAME;

    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << files_hashes;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

FilesHashes Protocol::GetSameFilesHashes() {
    std::stringstream ss;
    ss << last_received_.body;

    FilesHashes list;
    boost::archive::text_iarchive archive(ss);
    archive >> list;
    return list;
}

void Protocol::SendDifFileList(const FileList& dif_files) {
    Frame f;
    f.msg_id = MsgId::DIF_FILES;

    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << dif_files;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

void Protocol::SendFileDescr(const FileDescr file_info) {
    Frame f;
    f.msg_id = MsgId::FILE_DESCR;

    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << file_info;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

FileDescr Protocol::GetFileDescr() {
    std::stringstream ss;
    ss << last_received_.body;

    FileDescr file_descr;
    boost::archive::text_iarchive archive(ss);
    archive >> file_descr;
    return file_descr;
}

void Protocol::SendFileCreatedOk() {
    Frame f;
    f.msg_id = MsgId::FILE_CREATED_OK;
    conn_->WriteFrame(&f);
}

void Protocol::SendFilePart(const Frame file_part) {
    Frame f;
    f.msg_id = MsgId::FILE_PART;

    std::stringstream ss;
    boost::archive::text_oarchive archive(ss);
    archive << file_part;
    f.body = ss.str();
    conn_->WriteFrame(&f);
}

Frame Protocol::GetFilePart() {
    std::stringstream ss;
    ss << last_received_.body;

    Frame file_part;
    boost::archive::text_iarchive archive(ss);
    archive >> file_part;
    return file_part;
}

void Protocol::SendFilePartOk() {
    Frame f;
    f.msg_id = MsgId::FILE_PART_OK;
    conn_->WriteFrame(&f);
}

void Protocol::SendOk() {
    Frame f;
    f.msg_id = MsgId::OK;
    conn_->WriteFrame(&f);
}

MsgId Protocol::RecvMsg() {
    conn_->ReadFrame(&last_received_);
    return last_received_.msg_id;
}

Frame Protocol::LastFrame() {
    conn_->ReadFrame(&last_received_);
    return last_received_;
}

void read_all(int fd, const char* buf, size_t size) {
    int check_read_size = 0;
    while(check_read_size < size) {
        ssize_t r = read(fd, (void *)(buf + check_read_size), size - check_read_size);
        if(r == -1) {
            throw std::runtime_error("Reading error - connection faild");
        }
        check_read_size += r;
    }
};

void SocketConnection::ReadFrame(Frame* frame) {
    int length;
    const char * buf = (const char *) &length;
    read_all(fd_, buf, 4);

    unsigned char msg_id;
    buf = (const char *) &msg_id;
    read_all(fd_, buf, sizeof(msg_id));
    frame->msg_id = (MsgId)msg_id;

    int flag;
    buf = (const char *) &flag;
    read_all(fd_, buf, sizeof(flag));
    frame->last = (bool)flag;

    size_t uns_length = (size_t) length;
    frame->body.resize(uns_length);
    buf = frame->body.c_str();
    read_all(fd_, buf, uns_length);

    //std::cerr << "<< " << (int)frame->msg_id << std::endl;
}

void write_all(int fd, const char* buf, size_t size) {
    int check_writen_size = 0;
    while(check_writen_size < size) {
        ssize_t w = write(fd, buf + check_writen_size, size - check_writen_size);
        if(w == -1) {
            throw std::system_error(errno, std::system_category(), "Writting error - connection faild");
        }
        check_writen_size += w;
    }
};

void SocketConnection::WriteFrame(Frame* frame) {
    int length = frame->body.size(); //INCLUDING THE 4 bytes msg & flag
    const char * buf = (const char *) &length;
    write_all(fd_, buf, sizeof(length));

    unsigned char msg_id = (unsigned char)frame->msg_id;
    buf = (const char *) &msg_id;
    write_all(fd_, buf, sizeof(msg_id));

    int flag = frame->last;
    buf = (const char *) &flag;
    write_all(fd_, buf, sizeof(flag));

    write_all(fd_, frame->body.c_str(), frame->body.size());

    //std::cerr << ">> " << (int)frame->msg_id << std::endl;
}

FileList DirectoryList(std::string pathname) {
    DIR *d = opendir(pathname.c_str());
    if (!d) { exit(1); }

    struct dirent *dd;
    std::vector <std::string> files_names;

    while ((dd = readdir(d)) != NULL) {
        const char *f_name = dd->d_name;
        if (std::string(f_name) != "." && std::string(f_name) != "..") { // if no dirs starts with '.'
            files_names.push_back(std::string(f_name));
        }
    }

    std::sort(files_names.begin(), files_names.end(), [](std::string a, std::string b) { // SORT THE FILES - comparison function object, which returns Ã¢â‚¬â€¹true if the first argument is less than (i.e. is ordered before) the second.
        if (strcasecmp(a.c_str(), b.c_str()) < 0) { return true; }
        else { return false; }
    });

    closedir(d);

    FileList dir_files;
    dir_files.files = files_names;

    return dir_files;
}

std::vector<FileList> CheckDirFileList(FileList send_file_list, FileList *rm, FileList *cp, FileList *same, FileList rec_file_list) {
    // Check what files are different. We want to make the dir on the receiver to be the same as on the sender

    int send_file_num = 0;
    int rec_file_num = 0;

    while (send_file_num < send_file_list.files.size()) {
        if ( rec_file_num >= rec_file_list.files.size()  ) {
            while (send_file_num < send_file_list.files.size()) {
                cp->files.push_back(send_file_list.files[send_file_num]);
                send_file_num++;
            }
            break;
        }

        std::string send_file = send_file_list.files[send_file_num];
        std::string rec_file = rec_file_list.files[rec_file_num];

        if (send_file  == rec_file) {
            same->files.push_back(send_file);
            rec_file_num++;
            send_file_num++;
        } else if (strcasecmp(send_file.c_str(), rec_file.c_str()) < 0) { // send_file < rec_file
            cp->files.push_back(send_file);
            send_file_num++;
        } else if (strcasecmp(send_file.c_str(), rec_file.c_str()) > 0) {
            rm->files.push_back(rec_file);
            rec_file_num++;
        }
    }

    while (rec_file_num < rec_file_list.files.size()) { // if there are files left on the receiver that don't exist on the sender
        rm->files.push_back(rec_file_list.files[rec_file_num]);
        rec_file_num++;
    }

    std::vector<FileList> rm_cp_same_file_listes(3);
    rm_cp_same_file_listes[0] = *rm;
    rm_cp_same_file_listes[1] = *cp;
    rm_cp_same_file_listes[2] = *same;

    return rm_cp_same_file_listes;
}

std::vector<std::string> PrintComToSyncDirs(std::vector<FileList> rm_cp_same_file_lists) {
    std::string command;
    std::vector<std::string> commands;

    std::cout << "Need to be REMOVED (rm) from reciever:" << std::endl;
    for (std::string file_name : rm_cp_same_file_lists[0].files) {
        command = "rm " + file_name;
        std::cout << command << std::endl;
        commands.push_back(command);
    }
    if(!rm_cp_same_file_lists[0].files.size()) { std::cout << "None" << std::endl; }

    std::cout << "Need to be COPIED (cp) from the sender to reciever:" << std::endl;
    for (std::string file_name : rm_cp_same_file_lists[1].files) {
        command = "cp " + file_name;
        std::cout << command << std::endl;
        commands.push_back(command);
    }
    if(!rm_cp_same_file_lists[1].files.size()) { std::cout << "None" << std::endl; }
    std::cout << "SAME files:" << std::endl;
    for (std::string file_name : rm_cp_same_file_lists[2].files) {
        std::cout << file_name << std::endl;
        commands.push_back(command);
    }
    if(!rm_cp_same_file_lists[2].files.size()) { std::cout << "None" << std::endl; }

    return commands;
}

void Remove(FileList rm_file_lists, std::string path) {
    std::string full_path;
    std::cout << "Need to be REMOVED (rm) from reciever:" << std::endl;
    if(!rm_file_lists.files.size()) { std::cout << "None" << std::endl; }
    for (std::string file_name : rm_file_lists.files) {
        full_path = path + "/" + file_name;
        std::cout << "rm " << full_path << std::endl;
        if( remove(full_path.c_str()) != 0 ) {
            throw std::runtime_error("Error deleting file - connection faild");
        }
    }
}

void Frame::WriteFileToFrame(int fd) {
    size_t length = 4000;      //  PART_SIZE
    const char * buf;
    body.resize(length);
    buf = body.c_str();
    ssize_t read_b = read(fd, (void *)buf, length);
    if(read_b == -1) {
        throw std::runtime_error("Reading error - connection faild");
    }
    body.resize(read_b);

    if (read_b == 0) {
        last = true;
    }
}

void Frame::WriteFrameToFile(int fd) {
    const char* buf = body.c_str();
    size_t size = body.size();
    write_all(fd, buf, size);
}

void Protocol::CheckMsgError(MsgId check_msg) {
    MsgId cur_msg = RecvMsg();
    if (cur_msg != check_msg) {
        std::cout << "Last messege recived is " << (int)cur_msg << std::endl;
        throw std::runtime_error("Protocol messege error - connection faild");
    }
}

std::string FileHash(int fd) {
    MD5 hash_md5;
    char buf[32];
    size_t size = 32;
    ssize_t r_len = 0;
    while( ( r_len = read(fd, buf, size) ) > 0) {
        hash_md5.update(buf, r_len);
    }
    hash_md5.finalize();
    return hash_md5.hexdigest();
}

FilesHashes CountFilesHashes(std::vector<std::string> files_in_dir, std::string path_to_dir) {
    std::vector<std::vector<std::string>> names_hashes(2);
    for (std::string cur_file_name : files_in_dir) {
        std::string file_path = path_to_dir + "/" + cur_file_name;
        int cur_fd = open(file_path.c_str(), O_RDONLY, 0777);
        if (cur_fd == -1) {
            throw std::system_error(errno, std::system_category(), "Cannot open the same file on sender\n");
        }
        std::string cur_hash = FileHash(cur_fd);
        names_hashes[0].push_back(cur_file_name);
        names_hashes[1].push_back(cur_hash);
    }
    FilesHashes files_and_hashes;
    files_and_hashes.files_hashes = names_hashes;
    return files_and_hashes;
}

FileList CheckSameFilesHashes(FilesHashes same_files, std::string path_to_recv_dir) {
    FileList dif_files;
    for (int file = 0; file < same_files.files_hashes[0].size(); file++) {
        std::string cur_file_name = same_files.files_hashes[0][file];
        std::string send_hash = same_files.files_hashes[1][file];

        std::string file_path = path_to_recv_dir + "/" + cur_file_name;
        int rcv_fd = open(file_path.c_str(), O_RDONLY, 0777);
        if (rcv_fd < 0) {
            throw std::system_error(errno, std::system_category(), "Cannot open the same file on receiver\n");
        }
        std::string recv_hash = FileHash(rcv_fd);

        if(send_hash != recv_hash) {
            dif_files.files.push_back(cur_file_name);
            std::cout << "File " << cur_file_name << " was not up to date"  << std::endl;
        }
    }
    return dif_files;
}

void Sender::SendFiles(std::vector<std::string> file_names) {
    for (std::string file_name : file_names) {
        std::cout << "Start to COPY " << file_name << std::endl;
        FileDescr file_descr;
        file_descr.file_name = file_name;
        proto_.SendFileDescr(file_descr);
        proto_.CheckMsgError(MsgId::FILE_CREATED_OK);
        std::cout << "File Created ok" << std::endl;

        std::string file_path = path_ + "/" + file_name;
        int cur_fd = open(file_path.c_str(), O_RDONLY, 0777);
        if (cur_fd == -1) {
            throw std::system_error(errno, std::system_category(), "Cannot open file to copy from sender\n");
        }

        Frame file_part_frame;
        file_part_frame.last = false;
        while (!file_part_frame.last) {
            file_part_frame.WriteFileToFrame(cur_fd);
            proto_.SendFilePart(file_part_frame);
            proto_.CheckMsgError(MsgId::FILE_PART_OK);
        }
        close(cur_fd);
        std::cout << "File " << file_path <<  " is copied" << std::endl;
    }
}


void Sender::Work() {
    proto_.SendGetList();
    proto_.CheckMsgError(MsgId::FILELIST);
    FileList receiver_file_list = proto_.GetFileList();
    FileList sender_file_list = DirectoryList(path_);
    FileList rm, cp, same;
    CheckDirFileList(sender_file_list, &rm, &cp, &same, receiver_file_list);

    proto_.SendRemoveAndFileList(rm);
    proto_.CheckMsgError(MsgId::REMOVED_OK);

    FilesHashes same_files_hashes = CountFilesHashes(same.files, path_);
    proto_.SendSameFileHashes(same_files_hashes);
    proto_.CheckMsgError(MsgId::DIF_FILES);
    FileList dif_same_files = proto_.GetFileList();

    SendFiles(dif_same_files.files);
    SendFiles(cp.files);

    proto_.SendOk();
}


void Receiver::Work() {
    MsgId msg;
    while( true ) {
        msg = proto_.RecvMsg();

        if (msg ==  MsgId::GETLIST) {
            FileList dir_list = DirectoryList(path_);
            proto_.SendFileList(dir_list);
        }

        if (msg == MsgId::REMOVE) {
            FileList rm_file_list = proto_.GetFileList();
            Remove(rm_file_list, path_);
            proto_.SendRemovedOk();
        }

        if (msg == MsgId::CHECK_SAME) {
            FilesHashes same_files = proto_.GetSameFilesHashes();
            FileList dif_files = CheckSameFilesHashes(same_files, path_);
            proto_.SendDifFileList(dif_files);
        }

        while (msg == MsgId::FILE_DESCR) {
            FileDescr new_file = proto_.GetFileDescr();
            std::cout << "Start to COPY " << new_file.file_name << std::endl;

            std::string file_name = path_ + "/" + new_file.file_name;
            int new_fd = open(file_name.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0777);
            if (new_fd == -1) {
                throw std::system_error(errno, std::system_category(), "Cannot creat file\n");
            }
            proto_.SendFileCreatedOk();

            msg = proto_.RecvMsg();
            while (msg == MsgId::FILE_PART) {
                Frame file_part_frame = proto_.GetFilePart();
                file_part_frame.WriteFrameToFile(new_fd);
                proto_.SendFilePartOk();

                msg = proto_.RecvMsg();
            }

            close(new_fd);
            std::cout << "File " << file_name <<  " is copied" << std::endl;
        }

        if (msg == MsgId::OK) {
            break;
        }
    }
}