//
// Created by michal on 09.12.15.
//

#include "connection_types.h"
#include <sys/socket.h>
#include <thread>
#include <sys/types.h>          /* See NOTES */
#include <rsync.h>
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/wait.h>
#include <system_error>
#include <netdb.h>


void MakeLocalConnection(std::string src, std::string dst) {
    int socks[2];
    int sock_ok = socketpair(AF_UNIX, SOCK_STREAM, 0, socks);

    Sender sender(socks[0], src);
    Receiver receiver(socks[1],  dst);

    std::thread t1([&] {
        sender.Work();
    });

    std::thread t2([&] {
        receiver.Work();
    });
    t1.join();
    t2.join();
}

int MakeSendSocked (std::string host, std::string port) {
    struct addrinfo hints = {0};
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    struct addrinfo *res = NULL;
    if (getaddrinfo(host.c_str(), port.c_str(), &hints, &res) < 0) {
        throw std::system_error(errno, std::system_category(), "getaddrinfo failed\n");
    }

    std::shared_ptr<addrinfo> sh(res, freeaddrinfo);

    int fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        throw std::system_error(errno, std::system_category(), "socket failed\n");
    }
    if (connect(fd, res->ai_addr, res->ai_addrlen) < 0) {
        close(fd);
        throw std::system_error(errno, std::system_category(), "connect failed\n");
    }

    return fd;
}

void ConnectSender(std::string host, std::string port, std::string src_dir) {       //  client
    int fd_sock = MakeSendSocked (host, port);
    Sender sender(fd_sock, src_dir);
    sender.Work();
}


int MakeRcvSocked (std::string port_s) {
    int port = strtol(port_s.c_str(), 0, 10); // PORT

    if (port < 0 || port > 65535) {
        throw std::runtime_error("invalid port");
    }

    int fd = socket(PF_INET, SOCK_STREAM, 0);

    if (fd < 0) {
        throw std::system_error(errno, std::system_category(), "socket failed\n");
    }

    int opt = 1;
    setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

    struct sockaddr_in ba;
    ba.sin_family = AF_INET;
    ba.sin_port = htons(port);
    ba.sin_addr.s_addr = INADDR_ANY;
    if (bind(fd, (struct sockaddr*) &ba, sizeof(ba)) < 0) {
        close(fd);
        throw std::system_error(errno, std::system_category(), "sbind failed\n");
    }

    if (listen(fd, 16) < 0) {
        close(fd);
        throw std::system_error(errno, std::system_category(), "listen failed\n");
    }

    struct sockaddr_in aa;
    socklen_t slen = sizeof(aa);
    int afd = accept(fd, (struct sockaddr *) &aa, &slen);
    if (afd < 0) {
        close(fd);
        throw std::system_error(errno, std::system_category(), "accept failed\n");
    }

    close(fd);
    return afd;
}

void ConnectReceiver(std::string port_s, std::string dst_dir) {     //  server
    int fd_sock = MakeRcvSocked (port_s);

    Receiver receiver(fd_sock,  dst_dir);
    receiver.Work();
}