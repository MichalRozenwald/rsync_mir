# Введение

Для начала, небольшое напоминание. rsync - это утилита позволяющая
синхронизировать файлы между двумя машинами соединёнными сеть.

На каждой из машин запускается по процессу rsync-а, между ними
устанавливается TCP соединение. После того, как соединение
установлено, процессы обмениваются списками файлов и вычисляют какие
данные необходимо передать. После этого начинается фаза передачи данных.

Процесс rsync-а, принимающий входящие соединения, называется сервером.
Процесс rsync-а, устанавливающий соединение, называется клиентом.

Синхронизация может работать в 2 стороны, клиент => сервер и сервер =>
клиент. Чтобы не путаться, будем называть просылающий данные процесс -
Sender-ом, а принимающий - Receiver-ом.

# Протокол взаимодействия

На первом семинаре мы придумали протокол. На высоком уровне
взаимодействие между Sender-ом и Receiver-ом выглядит следующим
образом:

```


   *------*               *--------*
   |Sender|               |Receiver|
   *------*               *--------*
      ||    1) GETLIST        ||
      ||    ------------>     ||
      ||    2) FILELIST       ||
      ||    <------------     ||
      ||    3) REMOVE         ||
      ||    ------------>     ||
      ||    4) REMOVED_OK     ||
      ||    <------------     ||
      ||    5) CHECK_SAME     ||
      ||    ------------>     ||
      ||    6) DIF_FILES      ||
      ||    <------------     ||
      ||                      ||
      ||    7) FILE_DESCR     ||
      ||    ------------>     ||
      ||    8)FILE_CREATED_OK ||
      ||    <------------     ||
      ||    9) FILE_PART      ||
      ||    ------------>     ||
      ||    10) FILE_PART_OK  ||
      ||    <------------     ||
      ||   --REPEAT 5..8--    ||
      ||                      ||
      ||    11) OK            ||
      ||    ------------>     ||


////


   *------*                    *--------*
   |Sender|                    |Receiver|
   *------*                    *--------*
      ||    1) GETLIST             ||
      ||    ------------>          ||
      ||    2) FILELIST            ||
      ||    <------------          ||
      ||    3)REMOVE_CHECK_COPIES  ||
      ||    ------------>          ||
      ||    4) REMOVED_OK          ||
      ||    <------------          ||
      ||    5) DIFF_COPIES         ||
      ||    <------------          ||
      ||                           ||
      ||    6) FILE_DESCR          ||
      ||    ------------>          ||
      ||    7)FILE_CREATED_OK      ||
      ||    <------------          ||
      ||    8) FILE_PART           ||
      ||    ------------>          ||
      ||    9) FILE_PART_OK        ||
      ||    <------------          ||
      ||   --REPEAT 6..9--         ||
      ||                           ||
      ||    10) OK                 ||
      ||    ------------>          ||


enum class MsgId {
    GETLIST = 1,
    FILELIST = 2,
    REMOVE_CHECK_COPIES = 3,
    REMOVED_OK  = 4,
    DIFF_COPIES = 5,
    FILE_DESCR= 6,
    FILE_CREATED_OK = 7,
    FILE_PART = 8,
    FILE_PART_OK = 9,
    OK = 10
};
```

1) Sender посылает запрос на список файлов.

2) Receiver пересылает список файлов, которые есть у него.

3) Sender вычисляет, какие файлы нужно удалить на Receiver-е и
посылает запрос REMOVE.

4) Receiver удаляет файлы и отвечает OK.

5) Sender начинает посылать файлы. Сначала он посылает сообщение FILE,
в котором содержится метаинформация о файле(имя, права доступа и
т.д.). Далее идут несколько сообщений FILE_PART.

6) После того, как Receiver получил все куски файла, он отвечает
сообщением OK.

В сети, каждое сообщение передаётся передаётся так:

```
   *-----------------*-----------------*----------------*--------------------*
   | length (4-bytes)| msg_id (1-bytes)| flags (3-bytes)| body (length-bytes)|
   *-----------------*-----------------*----------------*--------------------*
```

Поле msg_id содержит тип сообщения. В body лежит сериализованный объект.
Поле flags может понадобиться для того, чтобы сообщить о конце файла.

# boost::serialization

Для сериализации сообщений рекомендуется использовать
boost::serialization, однако вы можете использовать любой другой
формат на своё усмотрение.

boost::serialization позволяет сериализовать любой класс в бинарную
строку и потом десериализовать его обратно из бинарной строки. Из
коробки библиотека умеет сериализовывать типы из стандартной
библиотеки. Чтобы научить её сериализовывать какой-то класс, нужно в
этот класс добавить шаблонный метод serialize (пример смотрите в
классе FileList).
