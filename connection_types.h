//
// Created by michal on 09.12.15.
//

#include <string>

#ifndef HSE_SAMPLE_PROJECT_CONNECTION_TYPES_H
#define HSE_SAMPLE_PROJECT_CONNECTION_TYPES_H

void MakeLocalConnection (std::string src, std::string dst);

void ConnectSender (std::string host, std::string port, std::string src_dir); //client

void ConnectReceiver (std::string port, std::string dst_dir); //server

int MakeSendSocked (std::string host, std::string port) ;

int MakeRcvSocked (std::string port_s);

#endif //HSE_SAMPLE_PROJECT_CONNECTION_TYPES_H
